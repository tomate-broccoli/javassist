package org.example;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

public class AppMain {

    public static void main(String[] args) throws Exception{
        change();
    }

    public static void change() throws Exception {
        ClassPool cp = ClassPool.getDefault();
        CtClass ctClass = cp.get("org.mybatis.generator.internal.util.JavaBeansUtil");

        //NG: Exception in thread "main" java.lang.LinkageError: loader 'app' attempted duplicate class definition for org.mybatis.generator.internal.util.JavaBeansUtil. (org.mybatis.generator.internal.util.JavaBeansUtil is in unnamed module of loader 'app')
        // ctClass.toClass(org.mybatis.generator.internal.util.JavaBeansUtil.class);

        //WARNING: An illegal reflective access operation has occurred
        // ctClass.toClass();

        //WARNING: An illegal reflective access operation has occurred
        Class thisClass = AppMain.class;
        ClassLoader      loader = thisClass.getClassLoader();
        java.security.ProtectionDomain domain = thisClass.getProtectionDomain();
        ctClass.toClass(loader, domain);
    }
}
