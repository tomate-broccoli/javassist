package example;

// import java.util.List;

// import org.mybatis.generator.api.PluginAdapter;
// import org.mybatis.generator.config.Context;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
// import org.mybatis.generator.internal.util.JavaBeansUtil;

public class AMain {

    public static void main(String[] args) throws Exception{
        change();
    }

    public static void change() throws Exception {
        System.out.println("** Tip030Plugin.change() called.");

        ClassPool cp = ClassPool.getDefault();
        // CtClass ctClass = cp.get("JavaBeansUtil");
        CtClass ctClass = cp.get("org.mybatis.generator.internal.util.JavaBeansUtil");
        CtMethod[] ctMethods = ctClass.getDeclaredMethods("getGetterMethodName");

        for(CtMethod ctMethod : ctMethods){
            CtClass[] parameterTypes = ctMethod.getParameterTypes();
            if(parameterTypes.length == 2){
                ctMethod.setBody("{ return sample.Tip030Util.getGetterMethodName($1, $2); }");
                break;
            }
            else {
                throw new Exception("target method not found.");
            }
        }
        // Class modifiedClass = ctClass.toClass();
        // ctClass.getClass();
        // ctClass.toClass(this.getClass().getClassLoader(), null);
        ctClass.toClass(org.mybatis.generator.internal.util.JavaBeansUtil.class);
    }
}
