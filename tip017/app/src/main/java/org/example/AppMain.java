package org.example;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

public class AppMain {

    public static void main(String[] args) throws Exception{
        change();
    }

    public static void change() throws Exception {
        ClassPool cp = ClassPool.getDefault();
        CtClass ctClass = cp.get("org.mybatis.generator.internal.util.JavaBeansUtil");
/*
        CtMethod[] ctMethods = ctClass.getDeclaredMethods("getGetterMethodName");

        for(CtMethod ctMethod : ctMethods){
            CtClass[] parameterTypes = ctMethod.getParameterTypes();
            if(parameterTypes.length == 2){
                ctMethod.setBody("{ return \"hoge\"; }");
                break;
            }
            else {
                throw new Exception("target method not found.");
            }
        }
*/
        // ctClass.toClass(org.mybatis.generator.internal.util.JavaBeansUtil.class);
        // クラスローダーに登録する
		Class thisClass = AppMain.class;
		ClassLoader      loader = thisClass.getClassLoader();
		java.security.ProtectionDomain domain = thisClass.getProtectionDomain();
		ctClass.toClass(loader, domain);
    }
}
